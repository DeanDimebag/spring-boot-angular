package com.example.library.DAO;

import com.example.library.DTO.ProfileDTO;
import com.example.library.entity.Role;
import com.example.library.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDAO.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    RowMapper<User> rowMapper = (resultSet, rowNum) -> {
        User user = new User();
        user.setUserId(resultSet.getInt("user_id"));
        user.setName(resultSet.getString("name"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        return user;
    };

    RowMapper<ProfileDTO> profileRowMapper = (resultSet, rowNum) -> {
        ProfileDTO profile = new ProfileDTO();
       profile.setName(resultSet.getString("name"));
       profile.setEmail(resultSet.getString("email"));
       profile.setRoles(resultSet.getString("role_name"));
        return profile;
    };

    public List<User> queryAllUsers() {
        LOGGER.info("UserDAO queryAllUsers()");
        String query = "SELECT * FROM TBL_USERS";
        List<User> users = namedParameterJdbcTemplate.query(query, rowMapper);
        return users;
    }

    public User queryInsertUser(User user) {
        LOGGER.info("UserDAO queryInsertUser()");
        String query = "insert into tbl_users " +
                " (user_id, name, email, password) " +
                " values " +
                " (:id, :name, :email, :password);";
        Map params = new HashMap();
        params.put("id", user.getUserId());
        params.put("name", user.getName());
        params.put("email", user.getEmail());
        params.put("password", user.getPassword());
        namedParameterJdbcTemplate.update(query, params);
        return user;
    }

    public String queryDeleteUser(int userId) {
        LOGGER.info("UserDAO queryDeleteUser()");
        System.out.println("USER ID : "+userId);
        String query = "DELETE FROM TBL_USERS WHERE USER_ID = :id";
        String query1 = "DELETE FROM TBL_USER_ROLE WHERE U_ID = :id";
        String query2 = "DELETE FROM TBL_USER_BOOK WHERE U_ID = :id";
        Map params = new HashMap();
        params.put("id", userId);
        namedParameterJdbcTemplate.update(query1, params);
        namedParameterJdbcTemplate.update(query2, params);
        namedParameterJdbcTemplate.update(query, params);
        return "USER ID : "+ userId;
    }

    public User queryUserById(int userId) {
        LOGGER.info("UserDAO queryUserById()");
        String query = "SELECT * FROM TBL_USERS WHERE USER_ID = :id";
        Map params = new HashMap();
        params.put("id", userId);
        User user = namedParameterJdbcTemplate.queryForObject(query, params, rowMapper);
        return user;
    }

    public User queryUpdateUser(User user) {
        LOGGER.info("UserDAO queryUpdateUser()");
        String query = "UPDATE TBL_USERS SET NAME = :name, " +
                " EMAIL = :email, PASSWORD= :password WHERE USER_ID = :id";
        Map params = new HashMap();
        params.put("id", user.getUserId());
        params.put("name", user.getName());
        params.put("email", user.getEmail());
        params.put("password",user.getPassword());
        namedParameterJdbcTemplate.update(query, params);
        return user;
    }

    public ProfileDTO queryGetProfile(String email) {
        String query = "SELECT USER_ID, NAME , EMAIL, ROLE_NAME FROM TBL_USERS U " +
                " JOIN " +
                " TBL_USER_ROLE UR ON U.USER_ID  = UR.U_ID WHERE EMAIL= :email";
        Map params = new HashMap();
        params.put("email", email);
        return namedParameterJdbcTemplate.queryForObject(query, params, profileRowMapper);
    }
}
