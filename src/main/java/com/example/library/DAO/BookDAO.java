package com.example.library.DAO;

import com.example.library.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BookDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    RowMapper<Book> rowMapper = (resultSet, i) -> {
        Book book = new Book();
        book.setBookTitle(resultSet.getString("book_title"));
        book.setAuthorName(resultSet.getString("author_name"));
        book.setISBN(resultSet.getString("ISBN"));
        book.setPublication(resultSet.getString("publication"));
        return  book;
    };


    public List<Book> queryAllBooks() {
        String query = "SELECT * FROM TBL_BOOKS";
        return namedParameterJdbcTemplate.query(query,rowMapper);
    }
    
    public List<Book> queryAllAvailableBooks() {
        String query = "select * FROM TBL_BOOKS WHERE ISBN NOT IN (SELECT isbn_id FROM TBL_USER_BOOK);";
        return namedParameterJdbcTemplate.query(query,rowMapper);
    }

    public Book queryBook(int isbnId){
        String query = "SELECT * FROM TBL_BOOKS WHERE ISBN = :isbn";
        Map params = new HashMap();
        params.put("isbn", isbnId);
        return namedParameterJdbcTemplate.queryForObject(query,params,rowMapper);
    }

    public Book queryInsertBook(Book book){
        String query = "INSERT INTO TBL_BOOKS (ISBN, BOOK_TITLE, AUTHOR_NAME, PUBLICATION) " +
                " VALUES (:isbn, :bookTitle, :authorName, :publication)";
        Map params = new HashMap();
        params.put("isbn", book.getISBN());
        params.put("bookTitle", book.getBookTitle());
        params.put("authorName", book.getAuthorName());
        params.put("publication", book.getPublication());
        namedParameterJdbcTemplate.update(query,params);
        return book;
    }

    public Book queryUpdateBook(Book book){
        String query = "UPDATE TBL_BOOKS SET BOOK_TITLE = :bookTitle, AUTHOR_NAME = :authorName, PUBLICATION = :publication" +
                " WHERE ISBN = :isbn";
        Map params = new HashMap();
        params.put("isbn", book.getISBN());
        params.put("bookTitle", book.getBookTitle());
        params.put("authorName", book.getAuthorName());
        params.put("publication", book.getPublication());
        namedParameterJdbcTemplate.update(query,params);
        return book;
    };

    public String queryDeleteBook(int isbn){
        String query = "DELETE FROM TBL_BOOKS WHERE ISBN = :isbn";
        Map params = new HashMap();
        params.put("isbn", isbn);
        namedParameterJdbcTemplate.update(query,params);
        return "ISBN ID : "+isbn;
    }

}
