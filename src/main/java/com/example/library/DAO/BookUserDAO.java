package com.example.library.DAO;

import com.example.library.DTO.UserBookDTO;
import com.example.library.entity.BookUser;
import com.example.library.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BookUserDAO implements RowMapper<BookUser>{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public BookUser mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = (new BeanPropertyRowMapper<>(User.class)).mapRow(rs,rowNum);
		BookUser bookUser = (new BeanPropertyRowMapper<>(BookUser.class)).mapRow(rs,rowNum);
		bookUser.setUser(user);
		return bookUser;
	}

//	RowMapper<BookUser> rowMapper = (resultSet, i) -> {
//		BookUser bookUser = new BookUser();
//		bookUser.setId(resultSet.getInt("book_id"));
//		bookUser.setUser(resultSet.getInt("u_id"));
//		bookUser.setBook(resultSet.getObject("ISBN", Book.class));
//		bookUser.setBorrowDate(resultSet.getDate("borrow_date"));
//		bookUser.setExpireDate(resultSet.getDate("expire_date"));
//		return  bookUser;
//	};

		RowMapper<UserBookDTO> reservedRowMapper = (resultSet, i) -> {
		UserBookDTO reserved = new UserBookDTO();
		reserved.setId(resultSet.getLong("id"));
		reserved.setIsbn(resultSet.getLong("isbn"));
		reserved.setBookTitle(resultSet.getString("book_title"));
		reserved.setAuthorName(resultSet.getString("author_name"));
		reserved.setPublication(resultSet.getString("publication"));
		reserved.setName(resultSet.getString("name"));
		reserved.setBorrowDate(resultSet.getDate("borrow_date"));
		reserved.setExpireDate(resultSet.getDate("expire_date"));
		return reserved;
	};

    
    public BookUser queryInsertUserBook(BookUser bookUser) {
		String query = "INSERT INTO TBL_USER_BOOK ( U_ID, ISBN_ID, BORROW_DATE, EXPIRE_DATE)"
				+ " VALUES ( :userId, :isbnId, :borrowDate, :expireDate)";
		System.out.println("ID : "+bookUser.getId());
		System.out.println("USER ID : "+bookUser.getUser().getUserId());
		System.out.println("BOOK ISBN : "+bookUser.getBook().getISBN());
		System.out.println("BORROW DATE : "+ bookUser.getBorrowDate());
		System.out.println("EXPIRE DATE : "+ bookUser.getExpireDate());
		Map params = new HashMap();
		params.put("id", bookUser.getId());
		params.put("userId", bookUser.getUser().getUserId());
		params.put("isbnId",bookUser.getBook().getISBN());
		params.put("borrowDate", bookUser.getBorrowDate());
		params.put("expireDate", bookUser.getExpireDate());
		namedParameterJdbcTemplate.update(query, params);
		return bookUser;
	}
	
	public BookUser queryUpdateUserBook(BookUser bookUser) {
		String query = "UPDATE TBL_USER_BOOK SET U_ID = :userId, ISBN_ID = :isbnId, BORROW_DATE = :borrowDate,"
				+ " EXPIRE_DATE = :expireDate WHERE ID = :id";
		Map params = new HashMap();
		params.put("id", bookUser.getId());
		params.put("userId", bookUser.getUser().getUserId());
		params.put("isbnId",bookUser.getBook().getISBN());
		params.put("borrowDate", bookUser.getBorrowDate());
		params.put("expireDate", bookUser.getExpireDate());
//		params.put("status", bookUser.getStatus().toString());
//		if(bookUser.getStatus().toString() == "RETURNED") {
//			System.out.println("AVAILABLE");
//		}
		namedParameterJdbcTemplate.update(query, params);
		return bookUser;
	}

	public List<Map<String, Object>> queryReservedUserBook() {
		String query = "select " +
				"ub.id as book_id, isbn,  author_name, book_title, publication, name, borrow_date, expire_date " +
				" from tbl_books b " +
				"join tbl_user_book ub on b.isbn = ub.isbn_id " +
				"join tbl_users u where u.user_id = ub.u_id;";
		return jdbcTemplate.queryForList(query);
//		return namedParameterJdbcTemplate.query(query, rowMapper);
//		return null;
				}


    public List<BookUser> queryBooksReserved(String email) {
		String query = "select id,book_title, isbn, author_name, publication, name, borrow_date, expire_date from tbl_users u " +
				" join tbl_user_book ub on u.user_id = ub.u_id " +
				" join tbl_books b on b.isbn= ub.isbn_id " +
				" where email = :email";
		Map params = new HashMap();
		params.put("email", email);

		return namedParameterJdbcTemplate.query(query,
				params,
				reservedRowMapper);
    }
}
