package com.example.library.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.DAO.BookUserDAO;
import com.example.library.entity.BookUser;
import com.example.library.repository.BookUserRepository;

@Service
public class BookUserService {
	
	@Autowired
	private BookUserDAO bookUserDAO;
	
	@Autowired
	private BookUserRepository bookUserRepository;
	
	public BookUser insertUserBook(BookUser bookUser) {
		return bookUserDAO.queryInsertUserBook(bookUser);
	}

	public List<BookUser> fetchAllUserBook() {
		return bookUserRepository.findAll();
	}
	
	public List<Map<String, Object>> fetchAllReservedUserBook() {
		return bookUserDAO.queryReservedUserBook();
	}
	
	public BookUser updateUserBook(BookUser bookUser) {
		return bookUserDAO.queryUpdateUserBook(bookUser);
	}

    public List<BookUser> findAllReservedBooks(String email) {
		return bookUserDAO.queryBooksReserved(email);
    }
}
