package com.example.library.service;

import com.example.library.DAO.BookDAO;
import com.example.library.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookDAO bookDAO;

    public List<Book> fetchAllBooks(){
        return bookDAO.queryAllBooks();
    }
    
    public List<Book> fetchAllAvailableBooks(){
        return bookDAO.queryAllAvailableBooks();
    }

    public Book fetchBook(int isbn){
        return bookDAO.queryBook(isbn);
    }

    public Book insertBook(Book book){
        return bookDAO.queryInsertBook(book);
    }

    public Book updateBook(Book book){
        return bookDAO.queryUpdateBook(book);
    }

    public String deleteBook(int isbn){
        return bookDAO.queryDeleteBook(isbn);
    }

}
