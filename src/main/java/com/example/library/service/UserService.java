package com.example.library.service;

import com.example.library.DAO.UserDAO;
import com.example.library.DTO.ProfileDTO;
import com.example.library.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public List<User> fetchAllUsers(){
        return userDAO.queryAllUsers();
    }

    public User fetchUser(int userId){
        return userDAO.queryUserById(userId);
    }

    public User insertUser(User user){
        return userDAO.queryInsertUser(user);
    }

    public User updateUser(User user){
        return userDAO.queryUpdateUser(user);
    }

    public String deleteUser(int userId){
        return userDAO.queryDeleteUser(userId);
    }

    public ProfileDTO findProfileByEmail(String email) {
        return userDAO.queryGetProfile(email);
    }
}
