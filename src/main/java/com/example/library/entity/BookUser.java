package com.example.library.entity;

import java.util.Date;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "tbl_user_book")
public class BookUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JoinColumn(name = "u_id", referencedColumnName = "user_id")
    @ManyToOne
    private User user;


    @JoinColumn(name = "isbn_id", referencedColumnName = "isbn")
    @ManyToOne
    private Book book;
    
    @Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date borrowDate;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date expireDate;
	
//	@Enumerated(EnumType.STRING)
//    private Status status;


    public BookUser(int id, Book book, Date borrowDate, Date expireDate) {
		super();
		this.id = id;
		this.book = book;
		this.borrowDate = borrowDate;
		this.expireDate = expireDate;
	}
	

    public BookUser() {
	}


	public int getId() {
        return id;
    }

	public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

	public Date getBorrowDate() {
		return borrowDate;
	}

	public void setBorrowDate(Date borrowDate) {
		this.borrowDate = borrowDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

//	public Status getStatus() {
//		return status;
//	}
//
//	public void setStatus(Status status) {
//		this.status = status;
//	}
    
    
}
