package com.example.library.entity;

public enum Status {

	BOOKED,
	AVAILABLE,
	RETURNED,
	NOT_RETURNED,
	TIME_EXCEEDED
	
}
