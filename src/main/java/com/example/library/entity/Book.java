package com.example.library.entity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_books")
public class Book {

    @Id
    @Column(name = "isbn")
    private String ISBN;
    private String bookTitle;
    private String authorName;
    private String publication;

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getPublication() {
        return publication;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }
}
