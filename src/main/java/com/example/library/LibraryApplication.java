package com.example.library;

import com.example.library.entity.BookUser;
import com.example.library.entity.Role;
import com.example.library.entity.User;
import com.example.library.repository.BookUserRepository;
import com.example.library.repository.RoleRepository;
import com.example.library.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Collections;

@SpringBootApplication
public class LibraryApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private BookUserRepository bookUserRepository;

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Role roleAdmin = new Role();
		roleAdmin.setName("ADMIN");
		roleRepository.save(roleAdmin);

		Role roleUser = new Role();
		roleUser.setName("USER");
		roleRepository.save(roleUser);

		User userAdmin = new User();
		userAdmin.setUserId(1);
		userAdmin.setName("Dipesh Tuladhar");
		userAdmin.setEmail("dipesh@gmail.com");
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("123"));
//		userAdmin.setPassword("123");
		userAdmin.setRoles(Collections.singletonList(roleRepository.findByName("ADMIN")));
		userRepository.save(userAdmin);
		
//		BookUser bookUser = new BookUser();
//		bookUser.setId(1);
//		bookUser.setBook(userRepository.findById(1).get());

	}
}
