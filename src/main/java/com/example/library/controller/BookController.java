package com.example.library.controller;

import com.example.library.entity.Book;
import com.example.library.repository.BookRepository;
import com.example.library.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/books")
public class BookController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private BookService bookService;

    @GetMapping
    public List<Book> getAllBook(){
        LOGGER.info("FETCHING LIST OF BOOKS :");
        return bookService.fetchAllBooks();
    }
    
    @GetMapping("/available")
    public List<Book> getAllAvailableBook(){
        LOGGER.info("FETCHING LIST OF BOOKS :");
        return bookService.fetchAllAvailableBooks();
    }

    @GetMapping("/{isbn}")
    public Book getBook(@PathVariable ("isbn") int isbn){
        return bookService.fetchBook(isbn);
    }

    @PostMapping
    public Book saveBook(@RequestBody Book book){
        return bookService.insertBook(book);
    }

    @PutMapping("/update/{id}")
    public Book getUpdateBook(@PathVariable ("id") int isbn, @RequestBody Book book){
        return bookService.updateBook(book);
    }

    @DeleteMapping("/delete/{isbn}")
    public String getDeleteBook(@PathVariable("isbn") int isbn){
        return bookService.deleteBook(isbn);
    }

}
