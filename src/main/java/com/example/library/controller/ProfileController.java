package com.example.library.controller;

import com.example.library.DTO.ProfileDTO;
import com.example.library.entity.Book;
import com.example.library.entity.BookUser;
import com.example.library.entity.User;
import com.example.library.repository.BookUserRepository;
import com.example.library.repository.UserRepository;
import com.example.library.service.BookUserService;
import com.example.library.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/profile")
public class ProfileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private  UserRepository userRepository;

    @Autowired
    private BookUserService bookUserService;

    @GetMapping
    public ProfileDTO getProfile(@RequestHeader("Email") String email){
        LOGGER.info("Inside Profile : "+ email);
        ProfileDTO profile = userService.findProfileByEmail(email);
        return profile;
    }

    @GetMapping("/books")
    public List<BookUser> getAllReservedBook(@RequestHeader("Email") String email){
        User user = userRepository.findByEmail(email);
        return bookUserService.findAllReservedBooks(email);
    }

}
