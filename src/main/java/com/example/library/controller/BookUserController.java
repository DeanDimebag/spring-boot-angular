package com.example.library.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.entity.BookUser;
import com.example.library.entity.User;
import com.example.library.repository.BookUserRepository;
import com.example.library.repository.UserRepository;
import com.example.library.service.BookUserService;

@CrossOrigin("*")
@RestController
@RequestMapping("/bookuser")
public class BookUserController {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);

	@Autowired
	private BookUserService bookUserService;
	
	@Autowired
	private BookUserRepository bookUserRepository;

	@Autowired
	private UserRepository userRepository;

	
//	@GetMapping
//	public String getHone() {
//		return "BOOK USER CONTROLLER";
//	}
	@GetMapping
	public List<BookUser> getAllUserBook(){
		LOGGER.info("FETCHING USER BOOK LIST");
		return bookUserService.fetchAllUserBook();
	}
	
	@GetMapping("/reserved")
	public List<Map<String, Object>> getAllReservedUserBook(){
		LOGGER.info("FETCHING USER BOOK LIST");
		return bookUserService.fetchAllReservedUserBook();
	}
	
	@PostMapping
	public BookUser getInsertUserBook(@RequestBody BookUser bookUser) {
		System.out.println("ID : "+bookUser.getId());
		System.out.println("Email Inside Insert : "+bookUser.getUser().getEmail());
		User user = userRepository.findByEmail(bookUser.getUser().getEmail());
		System.out.println("USER ID : "+user.getUserId());
		System.out.println("USER ID : "+user.getName());
		bookUser.setUser(user);
//		System.out.println("USER ID : "+bookUser.getUser().getUserId());
		System.out.println("BOOK ISBN : "+bookUser.getBook().getISBN());
		System.out.println("BORROW DATE : "+ bookUser.getBorrowDate());
		System.out.println("EXPIRE DATE : "+ bookUser.getExpireDate());
		System.out.println("---------------------------------------------");
		return bookUserService.insertUserBook(bookUser);
	}
	
	@PutMapping("/update")
	public BookUser getUpdateUserBook(@RequestBody BookUser bookUser) {
		System.out.println("ID : "+bookUser.getId());
		System.out.println("USER ID : "+bookUser.getUser().getUserId());
		System.out.println("BOOK ISBN : "+bookUser.getBook().getISBN());
		System.out.println("BORROW DATE : "+ bookUser.getBorrowDate());
		System.out.println("EXPIRE DATE : "+ bookUser.getExpireDate());
//		System.out.println("STATUS : "+ bookUser.getStatus().toString());
		System.out.println("---------------------------------------------");
		return bookUserService.updateUserBook(bookUser);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteBookUser(@PathVariable ("id") int id) {
		bookUserRepository.deleteById(id);
	}
	
}
