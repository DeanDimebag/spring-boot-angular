package com.example.library.controller;

import com.example.library.DTO.ProfileDTO;
import com.example.library.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/")
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private UserService userService;

    @GetMapping
    public String getHome(){
        LOGGER.info("INSIDE HOME :");
        return "Welcome Home";
    }

    @GetMapping("/basic")
    public ProfileDTO getAuthentication(@RequestHeader("Email") String email){
        LOGGER.info("INSIDE HOME :");
        ProfileDTO profile = userService.findProfileByEmail(email);
        return profile;
//        return "YOUR ARE AUTHENTICATE :";
    }

}
