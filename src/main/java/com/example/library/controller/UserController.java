package com.example.library.controller;

import com.example.library.entity.User;
import com.example.library.repository.UserRepository;
import com.example.library.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/users")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @GetMapping("/details")
    public List<User> getAllUserDetails(){
        LOGGER.info("FETCHING ALL USER :");
        return userRepository.findAll();
    }

    @GetMapping
    public List<User> getAllUsers(){
        return userService.fetchAllUsers();
    }

    @GetMapping("/list")
    public List<User> getUserWithRole(){
        return  userRepository.findAll();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") int userId){
        return userService.fetchUser(userId);
    }

    @PutMapping("/update")
    public User getUpdateUser(@RequestBody User user){
        return userService.updateUser(user);
    }

    @DeleteMapping("/delete/{id}")
    public String getremoveUser(@PathVariable("id")int userId){
        return userService.deleteUser(userId);
    }


}
