package com.example.library.configuration;

import com.example.library.auth.AuthUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthUserServices authUserServices;

//    @Autowired
//    AuthenticationSuccessHandler authenticationSuccessHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.cors();
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/users").hasAuthority("ADMIN")
                .antMatchers("/books", "/basic", "/profile").hasAnyAuthority("USER","ADMIN")
//                .antMatchers("/bookuser").hasAnyAuthority("USER")
                .antMatchers("/","/register**","/h2-console/**","/books","/books/available","/bookuser/reserved").permitAll()
                .anyRequest().authenticated()
               .and()
                .csrf().disable()
                .formLogin()
                .failureUrl("/login?error")
                .and()
                .logout().permitAll();

        http.headers().frameOptions().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public BCryptPasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(authUserServices);
        provider.setPasswordEncoder(getPasswordEncoder());
        return provider;
    }

}

